import pytest
from ..src.case_handlers import *


def get_path(filename):
    return "/home/osirv/pi_labs/pavlovic/se_lab7/tests/test_assets/"+ filename

def test_case_file_exists_true():
    """ Testireaj za putanju koja  postoji """
    path = get_path("file.html")    
    handler = CaseFileExistsHandler()
    assert handler.test(path) == True


def test_case_file_exists_fals():
    """ Testireaj za putanju koja ne postoji """
    path = get_path("file_a.html")
    handler = CaseFileExistsHandler()
    assert handler.test(path) == False

def test_case_file_exists_true_run():
    """testiraj kada file postoji hoce li ga pravilno ucitati"""
    path = get_path("file.html")
    handler=CaseFileExistsHandler()
    assert "This file exists" in handler.run(path)

def test_case_file_exists_false_run():
    """Testiraj kada file ne postoji a pokrenemo run"""
    path = get_path("file_a.html")
    handler = CaseFileExistsHandler()
    with pytest.raises(CaseError) as error:
        handler.run(path)

    assert error.value.error_code == 500




def test_case_file_not_exists_true():
    """testireano za putanju koja postoji"""
    path = get_path("file.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(path) == False


def test_case_file_not_exists_false():
    """testiraj putanju koja ne postoji"""
    path = get_path("filea.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(path) == True
